#!/bin/bash

orange=`tput setaf 3`
reset_color=`tput sgr0`

export ARCH=`uname -m`

echo "Running on ${orange}${ARCH}${reset_color}"

sudo docker run -it -d --rm \
       -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
       -e DISPLAY=$DISPLAY \
       -e QT_X11_NO_MITSHM=1 \
       -e XAUTHORITY \
       --net "host" \
       --privileged \
       --name radar sensum4d/radar:release

xhost +local:`docker inspect --format='{{ .Config.Hostname }}' radar`

sudo docker exec --user "docker_radar" -it radar /bin/bash \
            -c "source /opt/ros/noetic/setup.sh;
                cd catkin_ws;
                source install/setup.bash;
                roslaunch sensum_radar radar_rviz.launch;
                /bin/bash;"
